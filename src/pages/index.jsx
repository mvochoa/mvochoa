import Image from "next/image"

import Logo from "../assets/logo.svg"

export default function Home() {
  return (
    <div className="flex flex-col justify-between items-center">
      <h1 className="text-3xl font-bold underline">
        Mvochoa
      </h1>
      <div className="min-h-screen mt-64 flex flex-col justify-end items-end">
        <Image className="inline-block h-12 w-12 rounded-full ring-2 ring-white bg-teal-700" src={Logo} alt="Mvochoa" />
      </div>
    </div>
  )
}
