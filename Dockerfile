FROM node:16-alpine as deps

WORKDIR /app

COPY package.json package-lock.json .
RUN npm install --production

FROM node:16-alpine as builder

WORKDIR /app
COPY . .
COPY --from=deps /app .

RUN npm install
RUN npm run build

FROM node:16-alpine

WORKDIR /app

ENV NODE_ENV production
ENV NEXT_TELEMETRY_DISABLED 1

RUN addgroup --system --gid 1001 nodejs
RUN adduser  --system --uid 1001 nextjs

COPY --from=deps    --chown=nextjs:nodejs /app/node_modules ./node_modules
COPY --from=builder --chown=nextjs:nodejs /app/package.json ./package.json
COPY --from=builder --chown=nextjs:nodejs /app/public ./public
COPY --from=builder --chown=nextjs:nodejs /app/.next ./.next
COPY --from=builder --chown=nextjs:nodejs /app/src ./src

USER nextjs

CMD ["npm", "start"]